import pytest

from permissions.controllers.controller_user import UserController

class TestControllerUser:
    @pytest.mark.django_db
    def test_get_test_user(self):
        user = UserController.get_test_user()
        assert user.username == "test_user"
