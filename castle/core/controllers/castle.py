class Castle:
    def __init__(self, user):
        self.user = user
        self.max_empty_places = 6
        self.empty_places = ['empty' for _ in range(self.max_empty_places)]
        self.building = []

    class FullPlace(Exception):
        def __str__(self):
            return "Место закончилось"

    def get_empty_place(self):
        return self.empty_places

    def add_building(self, building):
        if (not self.empty_places):
            raise self.FullPlace()
        self.building.append(building)
        self.empty_places.pop(0)

