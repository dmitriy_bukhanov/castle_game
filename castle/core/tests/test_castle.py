import pytest

from core.controllers.building_controller import BuildingController
from core.controllers.castle import Castle
from permissions.controllers.controller_user import UserController


@pytest.fixture
def user():
    return UserController.get_test_user()


@pytest.fixture
def castle():
    return Castle(UserController.get_test_user())


@pytest.fixture
def castle_id(castle):
    cstl_id = castle.save().id
    return cstl_id


class TestCastle:
    def test_create_castle(self, user):
        castle = Castle(user)
        assert type(castle) == Castle
        assert castle.user == user

    def test_place_for_build(self, castle):
        assert len(castle.get_empty_place()) > 1

    def test_add_building_for_castle(self, castle):
        building = BuildingController().create_building()
        count_place = len(castle.get_empty_place())
        castle.add_building(building)
        assert len(castle.get_empty_place()) + 1 == count_place

    def test_add_buildings_for_castle(self, castle):
        building = BuildingController().create_building()
        for i in range(6):
            castle.add_building(building)
        assert len(castle.get_empty_place()) == 0

    def test_add_buildings_for_full_castle(self, castle):
        building = BuildingController().create_building()
        for i in range(6):
            castle.add_building(building)
        try:
            castle.add_building(building)
        except Castle.FullPlace as e:
            assert str(e) == "Место закончилось"


class TestModelCastle:
    def test_save_castle(self, castle):
        castle_model = castle.save()
        assert castle_model == CastleModel.objects.get(id=castle_model.id)

    def test_load_castle(self, castle_id):
        castle = Castle(castle_id)
        assert len(castle.get_empty_place()) == 6
