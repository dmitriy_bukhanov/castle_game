

class UserController:
    def __init__(self, username):
        self.username = username
        pass

    @staticmethod
    def get_test_user():
        return UserController('test_user')
